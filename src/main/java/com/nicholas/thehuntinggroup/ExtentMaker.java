package com.nicholas.thehuntinggroup;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class ExtentMaker {

    public static void main(String[] args) {
        Permutations permutations = new Permutations(3);
        GenerateExtents(permutations);
    }

    public static boolean isValid(List<Integer> r1, List<Integer> r2) {
        for (int x : r1) {
            int location = r1.indexOf(x);
            if (location != r2.indexOf(x) && location + 1 != r2.indexOf(x)
                    && location - 1 != r2.indexOf(x)) {
                return false;
            }
        }

        return true;
    }

    public static Node GenerateExtents(Permutations permuter) {
        List<List<Integer>> myPermutations = permuter.getPermutations();

        int numPermutations = myPermutations.size();
        long maxDepth = factorial(permuter.getN());

        Node root = new Node(0); // The roots current index is the first row

        Stack<Node> nodesToExplore = new Stack<>();
        nodesToExplore.push(root);

        while (!nodesToExplore.isEmpty()) {
            Node currentNode = nodesToExplore.pop();

            for (int position = 1; position < numPermutations; position++) {
                List<Integer> potentialRow = myPermutations.get(position);
                List<Integer> currentRow = myPermutations.get(currentNode.index);

                if (!currentNode.isRowInPath(position)
                        && isValid(currentRow, potentialRow)) {
                    Node adjacentNode = currentNode.addAdjacentRow(position);
                    nodesToExplore.push(adjacentNode);

                    if (adjacentNode.path.size() == maxDepth - 1 && isValid(potentialRow, myPermutations.get(0))) {
                        List<List<Integer>> extent = getExtent(adjacentNode, permuter);
                        prettyPrintExtent(extent);
                    }
                }
            }

        }

        System.out.println("Extent Search Complete!!!!");
        return root;
    }

    public static long factorial(int n) {
        int result = n;

        while (n >= 3) {
            result *= --n;
        }

        return result;
    }

    public static List<List<Integer>> getExtent(Node finalRowNode, Permutations permuter) {
        List<List<Integer>> permutations = permuter.getPermutations();
        List<List<Integer>> extent = new ArrayList<>(permuter.getN());

        for (int index : finalRowNode.path) {
            extent.add(permutations.get(index));
        }

        extent.add(permutations.get(finalRowNode.index));
        extent.add(permutations.get(0));

        return extent;
    }

    public static void prettyPrintExtent(List<List<Integer>> extent) {
        System.out.println("*******************************************");
        for (List<Integer> row : extent) {
            System.out.println(row);
        }
        System.out.println("*******************************************\n");
    }

    static class Node {
        int index;
        List<Node> adjacentNodes = new ArrayList<>();
        Set<Integer> path = new LinkedHashSet<>();

        public Node(int index) {
            this.index = index;
        }

        public Node(int index, Node previous) {
            this(index);
            this.path.addAll(previous.path);
            this.path.add(previous.index);
        }

        public Node addAdjacentRow(int index) {
            Node adjacentNode = new Node(index, this);
            adjacentNodes.add(adjacentNode);

            return adjacentNode;
        }

        public boolean isRowInPath(int index) {
            return this.index == index || path.contains(index);
        }
    }
}