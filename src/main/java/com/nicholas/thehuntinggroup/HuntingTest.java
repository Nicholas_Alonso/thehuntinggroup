package com.nicholas.thehuntinggroup;

import java.util.ArrayList;

/**
 * Created by Nicholas on 2/9/2015.
 */
public class HuntingTest {

    public static void main(String[] args) {
        ArrayList<int[]> a = new ArrayList<>();
        int r1[] = {1,2,3};
        int r2[] = {2,1,3};
        int r3[] = {2,3,1};
        int r4[] = {3,2,1};
        int r5[] = {3,1,2};
        int r6[] = {1,3,2};
        int r7[] = {2,1,3};
        int r8[] = {2,1,3};
        a.add(0,r1);
        a.add(1,r2);
        a.add(2,r3);
        a.add(3,r4);
        a.add(4,r5);
        a.add(5,r6);
        a.add(6,r7);
        a.add(7,r8);

        if(checkExtent(a)) {
            System.out.println("SUCCESS");
        } else System.out.println("FAILURE");
    }
    public static boolean checkExtent(ArrayList<int[]> extentCheck) {
        int index = 0;
        boolean forwardCheck = false;
        for(int row = 0; row < extentCheck.size(); row++){
            if(!forwardCheck){
                if(extentCheck.get(row)[index] == 1) {
                    if(index == extentCheck.get(0).length - 1) {
                        forwardCheck = true;
                    } else index++;
                } else if(extentCheck.get(row)[0] == 1){
                    index = 1;
                } else index = 0;
            } else {
                if(extentCheck.get(row)[index] == 1) {
                    if(index == 0) {
                        return true;
                    } else index--;
                } else {
                    forwardCheck = false;
                    if(extentCheck.get(row)[0] == 1) {
                        index = 1;
                    } else index = 0;
                }
            }
        }
        return false;
    }
}