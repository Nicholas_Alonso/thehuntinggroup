package com.nicholas.thehuntinggroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Calculates all permutations of 1, 2, ..., n
 */
public class Permutations {
    private List<List<Integer>> permutations;
    private List<Integer> firstRow;

    private int n;

    public Permutations(int size) {
        n = size;

        List<Integer> row = new ArrayList<>();
        for (int i = 1; i <= size; i++) {
            row.add(i);
        }

        firstRow = new ArrayList<>(row);
        permutations = new ArrayList<>();

        // list all the permutations
        calculatePermutations(row);
    }
    public static void main(String[] args) {
        Permutations permutations = new Permutations(4);
        System.out.println(permutations.getPermutations());
    }
    /**
     *Calls the permute method starting with the first element of the list
     * @param startingRow A list of numbers from 1 to n
     */
    private void calculatePermutations(List<Integer> startingRow) {
        permute(startingRow, 0);
    }
    /**
     * First checks for the base case (nothing to permute)
     * Then swaps element with the first element of the list (including
     * the special case where the first element swaps with itself)
     * Then repeats the process starting on the next element of the list
     * until the base case is reached.
     * @param row row to be permuted
     * @param fixed the index of the list that each element is swapped with
     */
    private void permute(List<Integer> row, int fixed) {
        if (fixed >= row.size() - 1) {
            // nothing left to permute
            permutations.add(new ArrayList<>(row));
        } else {
            // put each element from fixed+1 to the end of the list in location fixed in turn

            // special case (no swaps) for leaving item in loc fixed as is
            permute(row, fixed + 1);

            // swaps each element with fixed
            int x = row.get(fixed);
            for (int i = fixed + 1; i < row.size(); i++) {
                // swap elements at locations fixed and i
                row.set(fixed, row.get(i));
                row.set(i, x);

                // find all permutations of the elements after fixed
                permute(row, fixed + 1);

                // put things back the way they were
                row.set(i, row.get(fixed));
                row.set(fixed, x);
            }
        }
    }
    public List<List<Integer>> getPermutations() {
        return permutations;
    }
    public int getN() {
        return n;
    }
}


